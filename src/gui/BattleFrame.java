// COMENTÀRIO IMPORTANTE NA LINHA 90
package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import estrutura.Batalhar;
import estrutura.Enemy;
import estrutura.Player;
public class BattleFrame{

    private Enemy enemy;
    private Player player;
    private JFrame frmBtl;

    public BattleFrame(Player player, Enemy enemy){
        this.player = player;
        this.enemy = enemy;
        this.initialize();

    }

    private void fecha(){
        //fecha frm battle
        //frmBtl.setVisible(false);
        frmBtl.dispose();

    }

    private void initialize(){

        //boolean quit = false;
		frmBtl= new JFrame();
		frmBtl.setTitle("AA comp2");
        frmBtl.setBounds(100, 100, 600, 400);
        //Não quero que feche
		frmBtl.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmBtl.getContentPane().setLayout(new CardLayout(0, 0));

        final JPanel mainPanel = new JPanel();

        //btns btlPanel-------------------------

        final JButton atacar = new JButton("Atacar");
		atacar.setFont(new Font("Monospaced", Font.BOLD, 14));
        atacar.setBackground(new Color(255, 153, 0));
        atacar.setForeground(Color.WHITE);
		atacar.setBounds(10, 203, 564, 60);

        atacar.addActionListener( new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
                Batalhar.ataca(enemy,player);
                //if( !(player.isAlive() && enemy.isAlive()) )
            }
        });

        atacar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				atacar.setBackground(new Color(200, 0, 0));
				atacar.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				atacar.setBackground(new Color(255, 0, 0));
				atacar.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			public void mouseReleased(MouseEvent arg0) {
                if( !(enemy.isAlive() && player.isAlive()) )
                    fecha();
		    }
        });

        final JButton pocao  = new JButton("Poção");
		pocao.setFont(new Font("Monospaced", Font.BOLD, 14));
        pocao.setBackground(new Color(255, 153, 0));
        pocao.setForeground(Color.WHITE);
		pocao.setBounds(10, 274, 564, 60);

        pocao.addActionListener( new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
                // ESSE 100 SERIA A QTD DE SANGUE QUE A POÇÂO ENCHE
                // ESTÀ 100 SOH PARA TESTE
                Batalhar.enchelife(player,100);
                //if( !(player.isAlive() && enemy.isAlive()) )
            }
        });

       pocao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
			    pocao.setBackground(new Color(200, 0, 0));
			    pocao.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
			    pocao.setBackground(new Color(255, 0, 0));
			    pocao.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			public void mouseReleased(MouseEvent arg0) {
                if( !(enemy.isAlive() && player.isAlive()) )
                    fecha();
		    }
       });


        mainPanel.add(atacar); mainPanel.add(pocao);
        frmBtl.add(mainPanel);
        atacar.setVisible(true); pocao.setVisible(true);
        mainPanel.setVisible(true);
        frmBtl.pack();
        frmBtl.setVisible(true);

        while(player.isAlive() && enemy.isAlive()){

        }

    }
}
