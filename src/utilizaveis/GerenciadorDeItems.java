package utilizaveis;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GerenciadorDeItems {
	private Arma[] armasDoJogo;
	private BufferedImage potionImg;
	public GerenciadorDeItems(){
        BufferedImage sabreImg = null;
        BufferedImage adagaImg = null;
        BufferedImage revolverImg = null;
        potionImg = null;
        armasDoJogo = new Arma[3];
		try {
			sabreImg = ImageIO.read(new File("imgs/sabre.png"));
			adagaImg = ImageIO.read(new File("imgs/adaga.png"));
			revolverImg = ImageIO.read(new File("imgs/revolver.png"));
			potionImg = ImageIO.read(new File("imgs/potion.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		armasDoJogo[0] = new Adaga(adagaImg,50,40,60);
		armasDoJogo[1] = new Sabre(sabreImg,100,90,50);
		armasDoJogo[2] = new Revolver(revolverImg,200,90,50);
	}
	public Arma getArmas(int pos){
		if(pos > 2 || pos < 0){
			System.out.println("Utilize numeros dentro do intervalo 0-2 para selecionar os items do jogo!");
			return null;
		}
		return armasDoJogo[pos];
	}
	public Potion getPotion(){
		return new Potion(potionImg);
	}
}
